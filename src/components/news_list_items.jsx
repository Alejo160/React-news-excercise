import React from "react";

const NewsItem = ({ item }) => {
  return (
    <div className="card">
      <div className="card-title">
        <span>{item.id} - </span>
        {item.title}
      </div>
      <p className="card-content">{item.feed}</p>
    </div>
  );
};

export default NewsItem;
