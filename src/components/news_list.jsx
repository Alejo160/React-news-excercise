import React from "react";
import NewsItem from "./news_list_items";

const NewsList = props => {
  let newsitems = props.news.map(item => {
    return <NewsItem key={item.id} item={item} />;
  });
  return (
    <div>
      {props.children}
      {newsitems}
    </div>
  );
};

export default NewsList;
