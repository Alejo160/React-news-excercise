import React, { Component } from "react";
import ReactDOM from "react-dom";

// Database Import
import JSON from "./db.json";

//Components
import Header from "./components/header";
import NewsList from "./components/news_list";

class App extends Component {
  state = {
    news: JSON,
    filtered: []
  };

  getKeyword = event => {
    // console.log(event.target.value);
    let keyword = event.target.value;
    let filtered = this.state.news.filter(item => {
      return item.title.indexOf(keyword) > -1;
    });
    this.setState({
      filtered
    });
    // console.log(filtered);
  };
  render() {
    let filteredN = this.state.filtered;
    let NewsSum = this.state.news;
    return (
      <div className="centered">
        <Header keywords={this.getKeyword} />
        <NewsList news={filteredN.length === 0 ? NewsSum : filteredN} />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
